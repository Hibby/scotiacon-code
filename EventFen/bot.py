from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
# pull in our token, channels, other secrets
import secrets
import logging
import json
import time
import urllib.request
from datetime import datetime
#from xml.dom import minidom

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

#open local schedule.
# can i make it not shit the bed if this doesn't exist?

with open('schedule.json', 'r') as f:
    print("opening local schedule")
    eventsLocal = json.load(f)

updater = Updater(secrets.BOT_CONFIG['token'])
jobqueue = updater.job_queue
dispatcher = updater.dispatcher

def remove_html(text):
    import re
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)


def fetch():
    # pull the schedule from the event schedule website
    url = 'https://schedule.scotiacon.org.uk/sc19/schedule/export/schedule.json'

    # TODO: This could check for the local Schedule's existence before it starts parsing it.

    req = urllib.request.Request(url)
    print("Fetching Remote Data")
    r = urllib.request.urlopen(req).read()
    eventsRemote = json.loads(r.decode('utf-8'))
    versRemote = eventsRemote['schedule']['version']
    versLocal = eventsLocal['schedule']['version']

## If local > remote, ignore
    if versLocal > versRemote:
        print("No changes, most recent schedule version is " + versLocal)
        return eventsLocal
## If remote > local, update
    elif versRemote > versLocal:
        print("Remote schedule is newer at version " + versRemote)
        urllib.request.urlretrieve('https://schedule.scotiacon.org.uk/sc19/schedule/export/schedule.json', 'schedule.json')
        

        # there's a whole chunk of code here for parsing the xml feed to announce changes
        # but the whitespace and newlines made it near impossible to actually 
        # parse in a sensible manner.
        # Future project!

        #changes = urllib.request.urlopen('https://schedule.scotiacon.org.uk/sc19/schedule/feed.xml')
        #xmldoc = minidom.parse(changes)
        #entries = xmldoc.getElementsByTagName('entry')
        #summary = entries[1].getElementsByTagName('summary')
        #for a in summary:
        #    text = a.childNodes[0].nodeValue
        #    print(text)
        #    text = remove_html(text)
        #    print(text)
        #    text = text.replace("\n","")
        #    print(text)
        #print(remove_html(entries[0].getElementsByTagName('summary').childNodes[0].nodeValue))
        #for s in entries:
        #    summaries = s.getElementsByTagName('summary')
        #    for a in summaries:
        #        print(remove_html(a.childNodes[0].nodeValue))
            #print(s.attributes['value'].value)

        return eventsRemote
    else:
## Local == remote, it's ok
        print("File's fine")    
    return eventsRemote

def parseSchedule(eventsreq):
    # Parse complex json into simple file I can understand to then feed to post system
    # cut each day into a single array, 0-2
    dayArray = []
    eventCounter = 0
    for i in range(eventsreq['schedule']['conference']['daysCount']):
        dayArray.append(eventsreq['schedule']['conference']['days'][i])

    print("EventFen is snuffling about in your files")
    # make a tider json object containing only events 
    # Hierarchy is:
    # Events [
    #       day {
    #            event name, event start, event day
    #           }
    # ]
    eventsList = { 'events' : []  }

    for i in range(eventsreq['schedule']['conference']['daysCount']):
        dayList = { 'days' : [] }
        for room in dayArray[i]['rooms']:
            for eventDetail in dayArray[i]['rooms'][room]:
                eventID = eventDetail['id']
                eventName = eventDetail['title']
                eventTime = datetime.strptime(eventsreq['schedule']['conference']['days'][i]['date'], "%Y-%m-%d")
                eventTime = eventTime.replace(hour=int(eventDetail['start'][0:2:]), minute=int(eventDetail['start'][3:6:]))
                eventPlace = eventDetail['room']
                eventURL = eventDetail['url']
                dayEvents = {
                    'id': eventID,
                    'name': eventName,
                    'starts': eventTime,
                    'day': i,
                    'place' : eventPlace,
                    'url' : eventURL
                    }
                dayList['days'].append(dayEvents)
                eventCounter += 1

        eventsList['events'].append(dayList['days'])
    print("Event count: " + str(eventCounter))
    return eventsList

#print("People allowed to use me:")
#for element in secrets.BOT_CONFIG['authorised']:
#    print(element)

def dayCheck(eventsreq):
    #today = datetime.now().date().strftime("%Y-%m-%d")
    today = datetime.now().date().strftime("%Y-%m-%d")
    for i in range(eventsreq['schedule']['conference']['daysCount']):
        eventDay = eventsreq['schedule']['conference']['days'][i]['date']
        if today == eventDay:
            return i
    return eventsreq['schedule']['conference']['daysCount'] + 1

# periodic function to populate list with events in next 5 minutes
def selectEvents(bot, events, full_list):
    upcoming_events = []
    #check the time, populate array with events in next 5 minutes
    now = datetime.now().time().strftime("%H:%M")
    print("I am woke")
    #What day of the con is it?
    today = dayCheck(full_list)
    #Iterate through today's events, select list
    if (today > full_list['schedule']['conference']['daysCount']):
        print("no con today")
#        bot.send_message(chat_id=secrets.BOT_CONFIG['channel'], text="No con today :(",
              #       parse_mode="Markdown")
    else:
        for i in range(len(events['events'][today])):
            startTime = events['events'][today][i]['starts']
            d = int(startTime.timestamp()) - datetime.now().timestamp()
            if 0 < int(d) <= 600:
                upcoming_events.append(events['events'][today][i])
            for i in range(len(upcoming_events)):
                msg = upcoming_events[i]
                postMsg(bot, msg)
            upcoming_events.clear()

def batchEvents(bot, events, full_list):
    upcoming_events = []
    #check the time, populate array with events in next 5 minutes
    now = datetime.now().time().strftime("%H:%M")
    print("I am woke")
    #What day of the con is it?
    today = dayCheck(full_list)
    #Iterate through today's events, select list
    if (today > full_list['schedule']['conference']['daysCount']):
        print("no con today")
     #   bot.send_message(chat_id=secrets.BOT_CONFIG['channel'], text="No con today :(",
      #               parse_mode="Markdown")
    else:
        for i in range(len(events['events'][today])):
            startTime = events['events'][today][i]['starts']
            d = int(startTime.timestamp()) - datetime.now().timestamp()
            # next 3 hours
            if 0 < int(d) <= 10800:
                upcoming_events.append(events['events'][today][i])
            for i in range(len(upcoming_events)):
                msg = upcoming_events[i]
                postMsg(bot, msg)
            upcoming_events.clear()

def postMsg(bot, msg):
#posttime = datetime.fromtimestamp(msg['startunix']).strftime('%H:%M')
    title = "It's event time!!"
    title += "\n*Event:* "+msg['name']
    title += "\n*Location:* "+msg['place']
    title += "\n*Start:* "+ msg['starts'].strftime("%H:%M")
    title += "\n*To learn more, head to:* "+msg['url']
    bot.send_message(chat_id=secrets.BOT_CONFIG['channel'], text=title,
                     parse_mode="Markdown")
    time.sleep(1)

# define my post functions
def start(bot, update): bot.send_message(chat_id=update.message.chat_id, text="I'm a fennec. We're scared of everything. Please don't talk to me!")

# this essentially replecates default bot behaviour
# but lets the bot itself be admin
def post_callback(bot, update, args):
    user_says = " ".join(args)
    print("from " + str(update.message.from_user.id))
    for element in secrets.BOT_CONFIG['authorised']:
        if (update.message.from_user.id == int(element)):
            bot.sendMessage(chat_id=secrets.BOT_CONFIG['channel'], text=user_says)

def status(bot, update): bot.send_message(chat_id=update.message.chat_id,
                                          text="Yes I can hear you Clem Fandango!")

# This runs every 10 minutes.
def event_announcer(bot, job):
    # Work out what events are next, drop into next_array
    events=fetch()
    eventsList=parseSchedule(events)
    selectEvents(bot, eventsList, events)

# new job to announce batches of events

def batch_announcer(bot, job):
    events=fetch()
    eventsList=parseSchedule(events)
    # if time == 10, 12, 15, 18, 21 then post events batch
    curhour = datetime.now().hour
    curMin = datetime.now().minute
    if (curhour == 9) or (curhour == 12) or (curhour == 15) or (curhour == 18) or (curhour == 21):  
        if curMin == 00:
            batchEvents(bot, eventsList, events)
    else: 
        print("It's not my time")

def unknown(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text="Sorry, I didn't understand that command.")

# Add function handlers
dispatcher.add_handler(CommandHandler('start', start))
dispatcher.add_handler(CommandHandler('post', post_callback, pass_args=True))
dispatcher.add_handler(CommandHandler('status', status))
dispatcher.add_handler(MessageHandler(Filters.command, unknown))
# set up announcements bot to run every 5 minutes
jobqueue.run_repeating(event_announcer, interval=600, first=0)
# jobqueue.run_repeating(batch_announcer, interval=60, first=0)

updater.start_polling()
