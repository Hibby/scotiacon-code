Various Projects for [Scotiacon](https://www.scotiacon.org.uk/) 2017 - 2019.

# EventFen

EventFen is a new, less-shit-than-last-year telegram shitposting robot to
announce upcoming events from schedule.json, and to allow certain users of
event staff to post announcements to an announcements channel. 

EventFen now has a secrets.py file, where I hide the secrets, and it parses
them as required. As secrets is in the .gitignore, you can't see it, but I have
included a new secrets.py.example file! The values are totally random in this,
please change them before using if you intend to reuse the code!

The token is your Telegram token, the channel is your announcements channel and
the authorised array is users who are allowed to interact with the robot!

## Operation

EventFen snuffles through the JSON output from our
[pretalx](https://pretalx.com) [schedule](https://schedule.scotiacon.org.uk/)

It posts to our Live Events channel what is happening next at the con. It works, but it's a little crude.

With 1-2 things happening in parallel, 

EventFen in future shall be interactive, and you can query for events and receive calendar files. 

Use @userinfobot to get the user ID of users you want to be adminned...
although that feature is functionally useless tbh!

Requires: Python3, [python-telegram-bot](https://python-telegram-bot.org/)
