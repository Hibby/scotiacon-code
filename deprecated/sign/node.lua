gl.setup(1920, 1080)

node.alias("room")

local json = require "json"

local saal = "Main Room"

local font1 = resource.load_font("captain.ttf")
local font2 = resource.load_font("ConvincingPirate.ttf")
local background = resource.load_image("resources/output.jpg")

util.auto_loader(_G)

util.file_watch("schedule.json", function(content)
    print("reloading schedule")
    talks = json.decode(content)
end)

util.file_watch("config.json", function(content)
	local config = json.decode(content)
	rooms = config.rooms
	room = config.rooms[saal]
end)

local base_time = N.base_time or 0
local current_talk
local all_talks = {}
local day = 0
local daytext = "empty"

function set_day(new_day)
	daytext = "null"
	if new_day == "0" then
		daytext = "Friday"
	elseif new_day == "1" then
		daytext = "Saturday"
	elseif day == "2" then
		daytext = "Sunday"
	else
		daytext = "uh"
	end
	-- print(daytext)
end

function check_day(time)
	if time > 1509843600 then
		return 2
	elseif time > 1509757201 then
		return 1
	elseif time > 1509670800 then
		return 0
	else
		return 0
	end
end
function get_now()
    return base_time + sys.now()
end

function check_next_talk()
    local now = get_now()
    local room_next = {}
    for idx, talk in ipairs(talks) do
        if rooms[talk.place] and not room_next[talk.place] and talk.unix + 25 * 60 > now then 
			room_next[talk.place] = talk
        end
    end

    for room, talk in pairs(room_next) do
        talk.lines = wrap(talk.title, 30)
    end

    if room_next[saal] then
        current_talk = room_next[saal]
    else
        current_talk = nil
    end

    all_talks = {}
    for idx, talk in ipairs(talks) do
		if #all_talks <= 10 then
        	if talk.place ~= room_next[saal] and check_day(talk.unix_end) == tonumber(day) and talk.unix + 25*60 > now then
				all_talks[#all_talks + 1] = talk
        	end
		end
    end
    for room, talk in pairs(all_talks) do
        talk.lines = wrap(talk.title, 27)
    end
    --for room, talk in pairs(room_next) do
    --    if room ~= current_talk.place then
    --       all_talks[#all_talks + 1] = talk
    --    end
    --end
    table.sort(all_talks, function(a, b) 
        if a.unix < b.unix then
            return true
        elseif a.unix > b.unix then
            return false
        else
            return a.place < b.place
        end
    end)
end

function wrap(str, limit, indent, indent1)
    limit = limit or 72
    local here = 1
    local wrapped = str:gsub("(%s+)()(%S+)()", function(sp, st, word, fi)
        if fi-here > limit then
            here = st
            return "\n"..word
        end
    end)
    local splitted = {}
    for token in string.gmatch(wrapped, "[^\n]+") do
        splitted[#splitted + 1] = token
    end
    return splitted
end

local clock = (function()
    local base_time = N.base_time or 0

    local function set(time)
        base_time = tonumber(time) - sys.now()
    end

    util.data_mapper{
        ["clock/midnight"] = function(since_midnight)
            set(since_midnight)
        end;
    }

    local left = 0

    local function get()
        local time = (base_time + sys.now()) % 86400
        return string.format("%d:%02d", math.floor(time / 3600), math.floor(time % 3600 / 60))
    end

    return {
        get = get;
        set = set;
    }
end)()

check_next_talk()

util.data_mapper{
    ["clock/set"] = function(time)
        base_time = tonumber(time) - sys.now()
        print("UPDATED TIME", base_time)
        N.base_time = base_time
        check_next_talk()
    end;
    ["clock/day"] = function(new_day)
        print("DAY", new_day)
        day = new_day
		set_day(new_day)
    end;
}

function switcher(screens)
    local current_idx = 1
    local current = screens[current_idx]
    local switch = sys.now() + current.time
    local switched = sys.now()

    local blend = 0.5
    
    local function draw()
        local now = sys.now()

        local percent = ((now - switched) / (switch - switched)) * 3.14129 * 2 - 3.14129
        progress:use{percent = percent}
        white:draw(WIDTH-50, HEIGHT-50, WIDTH-10, HEIGHT-10)
        progress:deactivate()

        if now - switched < blend then
            local delta = (switched - now) / blend
            gl.pushMatrix()
            gl.translate(WIDTH/2, 0)
            gl.rotate(270-90 * delta, 0, 1, 0)
            gl.translate(-WIDTH/2, 0)
            current.draw()
            gl.popMatrix()
        elseif now < switch - blend then
            current.draw(now - switched)
        elseif now < switch then
            local delta = 1 - (switch - now) / blend
            gl.pushMatrix()
            gl.translate(WIDTH/2, 0)
            gl.rotate(90 * delta, 0, 1, 0)
            gl.translate(-WIDTH/2, 0)
            current.draw()
            gl.popMatrix()
        else
            current_idx = current_idx + 1
            if current_idx > #screens then
                current_idx = 1
            end
            current = screens[current_idx]
            switch = now + current.time
            switched = now
        end
    end
    return {
        draw = draw;
    }
end

content = switcher{
    {
        time = 10;
        draw = function()
            font2:write(600, 200, "Other Rooms", 100, 0,0,0,0.9)
            white:draw(0, 300, WIDTH, 302, 0.6)
            y = 340
            local time_sep = false
            if #all_talks > 0 then
                for idx, talk in ipairs(all_talks) do
					if not time_sep and talk.unix > get_now() then
                        if idx > 1 then
                            y = y + 5
                            white:draw(0, y, WIDTH, y+2, 0.6)
                            y = y + 20
                        end
                        time_sep = true
                    end

                    local alpha = 1
                    if not time_sep then
                        alpha = 0.3
                    end
                    font2:write(250, y, talk.start, 50, 0,0,0,alpha)
                    font2:write(400, y, talk.place, 50, 0,0,0,alpha)
                    font2:write(690, y, talk.lines[math.floor((sys.now()/2) % #talk.lines)+1], 50, 0,0,0,alpha)
                	for i, speaker in ipairs(talk.speakers) do
                    	font2:write(1090 + i*250, y, speaker, 50, 0,0,0,alpha)
					end
					y = y + 60
                end
            else
                font2:write(400, 330, "No other talks.", 50, 1,1,1,1)
            end
        end
    }, {
        time = 30;
        draw = function()
            if not current_talk then
                font2:write(600, 200, "Next Talk", 100, 0,0,0,1)
                white:draw(0, 300, WIDTH, 302, 0.6)
                font2:write(400, 330, "Nope. That's it. Go to bed", 50, 0,0,0,1)
            else
                local delta = current_talk.unix - get_now()
                if delta > 0 then
                    font2:write(600, 200, "Next talk", 100, 0,0,0,1)
                else
                    font2:write(600, 200, "This talk", 100, 0,0,0,1)
                end
                white:draw(0, 300, WIDTH, 302, 0.6)

                font2:write(130, 330, current_talk.start, 100, 0,0,0,1)
                if delta > 0 then
					if delta > 3600 then
						font2:write(130,330+100, string.format("in %.3g h", delta/3600), 50, 0,0,0,0.8)
					else
                    	font2:write(130, 330 + 100, string.format("in %d min", math.floor(delta/60)+1), 50, 0,0,0,0.8)
					end
                end
                for idx, line in ipairs(current_talk.lines) do
                    if idx >= 5 then
                        break
                    end
                    font2:write(400, 330 - 60 + 60 * idx, line, 100, 0,0,0,1)
                end
				font2:write(400,450, "By", 100, 0,0,0,1)
                for i, speaker in ipairs(current_talk.speakers) do
                    font2:write(400, 500 + 100 * i, speaker, 100, 0,0,0,1)
                end
            end
        end
    }
}


function node.render()
	gl.clear(0.99,0.64,0.34,1)
	if base_time == 0 then
		return
	end
	background:draw(0, 0, WIDTH, HEIGHT)
	-- draw from x1,y1 to x2,y2

    local fov = math.atan2(HEIGHT, WIDTH*2) * 360 / math.pi
    gl.perspective(fov, WIDTH/2, HEIGHT/2, -WIDTH,
                        WIDTH/2, HEIGHT/2, 0)

	content.draw()
	--set_day()
	font1:write(120, 60, daytext, 160, 1,1,1,1)
	font1:write(900,60, saal, 160, 1,1,1,1)	
end
