gl.setup(1920, 1080)

node.alias("room")

local json = require "json"

local font1 = resource.load_font("Enchanted Land.otf")
local font2 = resource.load_font("ConvincingPirate.ttf")
local background = resource.load_image("SC2018 BACK GROUND.png")

util.auto_loader(_G)

util.file_watch("schedule.json", function(content)
    print("reloading schedule")
    talks = json.decode(content)
end)

local base_time = N.base_time or 0
local current_talk
local all_talks = {}
local day = 0
local daytext = "empty"
local current_hour = "nope"

function check_day(time)
	if time >  1541379600 then
		return 2
	elseif time > 1541289599 then
		return 1
	elseif time > 1541203199 then
		return 0
	else
		return 0
	end
end

function check_next_talk()
    local now = base_time
	all_talks = {}
    for idx, talk in ipairs(talks) do
		if #all_talks <= 10 then
			if check_day(talk.finishunix) == tonumber(day) and talk.startunix + 25*60 > now then
				all_talks[#all_talks + 1] = talk
				--  print (#all_talks .. " " .. talk.title)
        	end
		end
    end

-- handle wrapping of overlong things
    for idx, talk in pairs(all_talks) do
        talk.lines = wrap(talk.title, 31)
	talk.subloc = wrap(talk.place, 10)
    end
    
	table.sort(all_talks, function(a, b) 
        if a.startunix < b.startunix then
            return true
        elseif a.startunix > b.startunix then
            return false
        else
            return a.place < b.place
        end
    end)
end

function wrap(str, limit, indent, indent1)
    limit = limit or 72
    local here = 1
    local wrapped = str:gsub("(%s+)()(%S+)()", function(sp, st, word, fi)
        if fi-here > limit then
            here = st
            return "\n"..word
        end
    end)
    local splitted = {}
    for token in string.gmatch(wrapped, "[^\n]+") do
        splitted[#splitted + 1] = token
    end
    return splitted
end

check_next_talk()

util.data_mapper{
    ["clock/set"] = function(time)
        base_time = tonumber(time)
        print("UPDATED TIME", base_time)
        N.base_time = base_time
        check_next_talk()
    end;
    ["clock/day"] = function(new_day)
        print("DAY", new_day)
		-- day = new_day
		daytext = new_day
    end;
	["clock/hour"] = function(new_hour)
		current_hour = new_hour
	end;
}

function node.render()
	-- Set up background and basics
	gl.clear(0.99,0.64,0.34,1)
	background:draw(0, 0, WIDTH, HEIGHT)
    white:draw(0, 0, WIDTH, HEIGHT, 0.7)
	font1:write(120, 20, daytext .. " - " .. current_hour , 160, 0,0,0,alpha)
    font1:write(1200, 20, "Up Next", 160, 0,0,0,alpha)

    y = 250
    local time_sep = false
    if #all_talks > 0 then
        for idx, talk in ipairs(all_talks) do
            if not time_sep and talk.startunix > base_time then
                if idx > 1 then
                    y = y + 5
                    white:draw(0, y, WIDTH, y+2, 0.6)
                    y = y + 20
                    end
                time_sep = true
            end

            local alpha = 1
            if not time_sep then
                alpha = 0.3
            end
            font2:write(120, y, talk.startpretty, 50, 0,0,0,alpha)
            font2:write(270, y, talk.subloc[math.floor((sys.now()/2) % #talk.lines)+1), 50, 0,0,0,alpha)
            font2:write(560, y, talk.lines[math.floor((sys.now()/2) % #talk.lines)+1], 50, 0,0,0,alpha)
            font2:write(1300, y, talk.speakers, 50, 0,0,0,alpha)
            y = y + 60
        end
    else
        font2:write(400, 330, "No other talks.", 50, 1,1,1,1)
    end    
end
