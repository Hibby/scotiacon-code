Various Dead Projects for [Scotiacon](https://www.scotiacon.org.uk/) 2017 - 2019

# Sign

Sign is an information display based upon
[info-beamer](https://info-beamer.com/pi), and it runs on a [raspberry
pi](https://www.raspberrypi.org/).  The sign shows what events are coming up
next and what events are happening. 

Sign was deprecated as Xibo became easier, and info0beamer was becoming
increasingly hostile to work on the raspi/free version.

Sign has two variations - sign and lounge-sign, detailed below.

If you're runing this and testing it, I recommend you run the code on a
raspberry pi with an ssh server running. That way you can easily kill the code
if it's unresponsive.

## Sign

Sign will display what is happening in the room currently, what is on next and
what's happening elsewhere.

It looks a little like this:

![](./misc/sign-sample.jpg)

Sign was hacked together from the 30c3 info-beamer setup as I like that.  Then
most of that code was ignored, and what was left became a hilarious
frankenbaby. The originals live
[here](https://github.com/dividuum/info-beamer-nodes).

The application reads from schedule.json and populates the tables based on that
information.

It changes the displayed event list every 30s or so between what is happening
in the other rooms next and what is on in here just now and next.

It can be shown on screens in idle rooms, or as a filler on a livestream...

### Making sign run

You should be able to switch out backgrounds, fonts, schedule information and
setting the time to customise this for your event. 

The background and fonts are detailed in node.lua, the others are detailed
below:

#### schedule.json

Schedule.json is a simple file containing your whole event schedule.

Info-beamer helpfully rescans it every second so if you have a network
connection, you can quietly swap this file out as you please. 

Each event is an object:
```
{
  "speakers": ["Registration Team"],
  "unix": 1509703200,
  "unix_end": 1509712200,
  "title": "Registration",
  "start": "10:00",
  "place": "Main Room",
  "desc": "Get signed in to the con. ID Please!"
}
```

  * Speakers is an array containing those who present, but it can also be a
	single name. 

  * Unix and unix_end are start/finish times in unix time.

  * Title is self explanatory.

  * Start is the time in normal time (yes this could be calculated, I was
	stressed) 

  * Place is where it is happening.

  * Desc is the description. 

For the event, schedule.json was hand written. Whilst drunk. On Day 0. 

Note to future self: *DO NOT HAND WRITE SCHEDULE.JSON WHILE DRUNK*

##### Places!

Each Raspberry Pi has a unique serial number, and it should be able to
auto-identify what room it is in and display the right stuff(tm) based on that. 

Sadly this didn't get implemented, and as such you need to tell the card where
it is in the node.lua file.

What I'm working towards here is an image I can author once for many devices
and have autodetect the room it is in based on the raspberry pi and show the
right thing.

#### Service

Service is a python script which takes the current time and sends it to
info-beamer's UDP socket. 

If it's not running, your sign will display nothing for no apparent reason.

Because the raspberry pi is terrible and doesn't have a permanent Real Time
Clock, we need to use the bundled `service` script to tell the sign what
time it is.

In a nice future, I would like to use ntp or an RTC to set the time on boot and
simplify this cruft. I didn't have a reliable network, so this hack had to do. 

## Lounge-Sign

Lounge-sign will just display upcoming events. It was used in the Scotiacon
Lounge to display upcoming events. It's designed to be glance-and-go, and is
smart enough to clear out old events from the display.

The application reads from schedule.json and populates the tables based on that
and is subject to the real time issue as previously detailed.

### Making lounge-sign run

You should be able to switch out backgrounds, fonts, schedule information and
setting the time to customise this for your event. 

The background and fonts are detailed in node.lua, the others are detailed
in the sign section above.

## How to Make your Own Sign!

Below are some vague steps I took to hack together an autobooting sign. 

We need to make some changes to the raspbian image, so I suggest you make a
'clean' copy of it before you start any of this nonsense. 

To build a sign into a raspberry pi image, first download the [raspbian
lite](https://www.raspberrypi.org/downloads/raspbian/) image.

### Mount the Image

Mount the Raspbian image as a loop device locally follwing some of these
instructions:

http://blog.videgro.net/2015/11/modify-disk-image-raspbian/

Note: because I'm lazy, I Didn't make zsh do inline arithmetic, I calculated my
sectors first, but the end result was the same.

### Modify the Image

At this point, we're going to install info-beamer and its dependencies, and
then make it run on boot. Info-beamer renders directly with the graphics
hardware, no X Server is needed (hence the raspbian lite image).

To Modify the Raspbian Image, you will need to follow the below:

https://wiki.debian.org/RaspberryPi/qemu-user-static

I highly recommend installing `systemd-container` on your machine first, as
this was a nice way to run the chroot. It automounts all the /dev/ and /proc
nonsense so you don't ned to think about it. 

SystemD isn't so bad after all. 

`chroot` or `systemd-nspawn` into the image. Install:

`apt install libevent-2.0-5 libavformat57 libpng12-0 libfreetype6
libavresample3 python-tz`

Then install download and unzip info-beamer. I highly recommend sticking this
in either /bin or ~/bin.

Copy over your info-beamer code to the pi and follow the below to make it boot. 

https://info-beamer.com/blog/running-info-beamer-in-production

I had two services: one for infobeamer running the sign code, and one for
service setting the clock.

This meant I had to manually edit the service file
before every boot... It was fine until the cleaners turned it off. 

If there is demand, I can probably upload examples of the files required from
my archives.

### Copy the image

Unmount the modified raspbian image and dd it to an SD card. 
